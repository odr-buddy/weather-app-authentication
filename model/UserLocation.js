const mongoose = require("mongoose");

const userLocationsSchema = new mongoose.Schema(
    {
       userEmail:{
           type:String,
           max:50
       },
       location:[{
            city:{
                type:String
            },
            state:{
                type:String
            },
            country:{
                type:String
            },
            lat:{
                type:Number
            },
            long:{
                type:Number
            },
            favourite:{
                type:Boolean
            }
       }]
    }
);

module.exports = mongoose.model("userLocations", userLocationsSchema);