const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const locationRoute = require('./routes/location');
const cors = require('cors');

//Import Routes
const authRoute = require('./routes/auth');

dotenv.config();

//Connect to db
mongoose.connect(
  "mongodb+srv://weatherAppAdmin:AdminPassword@cluster0.tci1d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", 
  { useNewUrlParser: true},
  () => console.log('connected to db!') 
);

//Middleware
app.use(cors());
app.use(express.json());

//Route middlewares
app.use('/api/auth', authRoute);
app.use('/api/location', locationRoute);
app.get('/', (req, res) => {
  res.send('Hello from App Engine!');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log('Server up and running on port :' + PORT));

