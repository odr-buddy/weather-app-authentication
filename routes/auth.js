const router = require('express').Router();
const {userRegister, userLogin} = require('../utils/AuthUtils');


//api/user/register
router.post('/register', async (req, res) => {
  await userRegister(req.body, "user", res);
});

//api/user/login
router.post('/login', async (req, res) => {
  await userLogin(req.body, "user", res);
});

router.post('/refresh', (req, res) => {
  //For more security
});

module.exports = router;