const router = require('express').Router();
const Location = require("../model/UserLocation");
const verify = require('./verifyToken');
const {updateLocation, getLocation} = require('../utils/LocationUtils');


router.get('/', verify, async (req, res) => {
  await getLocation(req.headers, res);
});

router.put('/', verify, async  (req, res) => {
  await updateLocation(req.body, req.headers, res);
});


module.exports = router;

