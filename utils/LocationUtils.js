const {locationValidation} = require('../validation');
const jwt = require('jsonwebtoken');
const Location = require("../model/UserLocation");


const updateLocation = async(body, header, res) =>
{
    const {error} = locationValidation(body);
    if(error){
        return res.status(400).send(error.details[0].message);
    }
    try{
        const decodedToken = jwt.verify(header.authtoken, "dflkhsdfufn3f38f3fas");
        console.log(decodedToken);
        const location = await Location.findOne({userEmail : decodedToken.email});
        console.log(location);
        if (location == null){
          console.log("Create Entry");
          const newLocation = new Location({
            userEmail: decodedToken.email,
            location: body.location
          })
          const savedLocation = await newLocation.save();
          res.send(savedLocation);
        }
        else{
          console.log("Update Entry");
          await location.updateOne({$set:{location: body.location}});
          res.send({
            userEmail: decodedToken.email,
            location: body.location
          });
        }
      }catch(err){
        console.log(err);
        res.status(400).send(err);
      }
}

const getLocation = async(header, res) =>
{
    console.log(header);
    try{
        const decodedToken = jwt.verify(header.authtoken, "dflkhsdfufn3f38f3fas");
        const location = await Location.findOne({userEmail : decodedToken.email});
        res.status(200).send(location);
      }catch(err){
        res.status(400).send(err);
      }
}

module.exports = {
    updateLocation,
    getLocation
  };