const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../model/User');
const {registerValidation, loginValidation} = require('../validation');

const userRegister = async (userDets, role, res) => {
  const {error} = registerValidation(userDets);
  if(error) {
    return res.status(400).send(error.details[0].message);
  }

  //check if user exists
  const emailExists = await User.findOne({email: userDets.email});
  if(emailExists) return res.status(400).send('Email already exists');

  //Hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(userDets.password, salt);

  //create a new user
  const user = new User({
    name: userDets.name,
    email: userDets.email,
    password: hashedPassword,
    role: role
  });


  //saving a user
  try{
    const savedUser = await user.save();
    //Create and assign a token
   const token = jwt.sign({_id: user._id, email: user.email}, "dflkhsdfufn3f38f3fas");
   return res.send(token);
  }catch(err){
    return res.status(400).send(err);
  }
};

const userLogin = async (userDets, role, res) => {
   //Validate data
   const {error} = loginValidation(userDets);
   if(error) {
     return res.status(400).send(error.details[0].message);
   }
 
   //check if user exists
   const user = await User.findOne({email: userDets.email});
   if(!user) return res.status(400).send('This user email does not exist');
 
   //check the user role matches
  //  if(userDets.role !== user.role) return res.status(403).send('Log in using the correct portal');
 
   //check if password is correct
   const validPass = await bcrypt.compare(userDets.password, user.password);
   if(!validPass) return res.status(400).send('Invalid password');
 
   //Create and assign a token
   const token = jwt.sign({_id: user._id, email: user.email}, "dflkhsdfufn3f38f3fas");
   return res.send(token);
  //  return res.header('auth-token', token).send(`Bearer ${token}`);
}

module.exports = {
  userRegister,
  userLogin
};